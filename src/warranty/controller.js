
const warModel = require('./model')


const name = () => async (req, res, next) => {
    try {
        let respon = await warModel.test()

        req.success = respon
        req.message = ''
        req.status = 200
    } catch (error) {
        req.success = []
        req.message = error
        req.status = 500
    }
    next()
}





module.exports = {
    name
}