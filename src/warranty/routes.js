const express = require('express');

const router = express.Router();
const controller = require('./controller')

router.post('/',

    controller.name(),
    (req, res) => {
        res.status(200).json({ success: req.success, message: req.message })
    })

module.exports = router;