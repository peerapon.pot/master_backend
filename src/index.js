const warrantyRoute = require('./warranty/routes')

module.exports = (app) => {
    app.use('/api/warranty', warrantyRoute)
}